# the-meal-wheel
This project is made in Vue3. It is test project with an interesting idea and sole purpose of learning new technology.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## TODO List
- ### Components
- ### Functionalities
- Meal Wheel for random restaurant
- List of restaurants (Husinec, Ferdinanda, Příčný řez, Šenkovna, u Pravdů, the Pub, Jáma Pub) -> adresa, detail daily menu (mapa k restauraci), links
- Search by Restaurant
- Search by Meal
- Swipe to spin on mobile
