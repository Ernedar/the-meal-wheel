import { defineComponent } from 'vue';

export default defineComponent({
  name: 'BackLink',
  props: {
    whereTo: {
      type: String,
      default: 'MealWheel',
    },
  },
});

