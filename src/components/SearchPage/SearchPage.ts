import { defineComponent } from 'vue';

import IconChicken from '@/components/Icons/IconChicken/IconChicken.vue';
import IconRestaurant from '@/components/Icons/IconRestaurant/IconRestaurant.vue';

export default defineComponent({
  name: 'SearchPage',
  components: {
    IconChicken,
    IconRestaurant,
  },
});

