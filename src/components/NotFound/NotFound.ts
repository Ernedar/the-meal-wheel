import { defineComponent } from 'vue';
import IconMeal from '@/components/Icons/IconMeal/IconMeal.vue';

export default defineComponent({
  name: 'NotFound',
  components: {
    IconMeal,
  },
});

