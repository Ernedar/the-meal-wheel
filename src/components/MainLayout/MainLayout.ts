import { defineComponent } from 'vue';
import { DailyMenuLists } from '../../utils/interface';

export default defineComponent({
  name: 'MainLayout',
  data() {
    return {
      dailyMenusData: {} as DailyMenuLists,
    };
  },
});
