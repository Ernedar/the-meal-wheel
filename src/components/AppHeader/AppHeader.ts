import { defineComponent } from 'vue';

import IconKnifeAndFork from '../Icons/IconKnifeAndFork/IconKnifeAndFork.vue';

export default defineComponent({
  name: 'AppHeader',
  components: {
    IconKnifeAndFork,
  },
});

