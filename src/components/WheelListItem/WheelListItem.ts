import { defineComponent } from 'vue';

export default defineComponent({
  name: 'WheelListItem',
  props: {
    itemName: String,
    itemId: Number,
  },
});

