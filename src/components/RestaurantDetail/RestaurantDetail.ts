import { defineComponent } from 'vue';

import IconExternalLink from '../Icons/IconExternalLink/IconExternalLink.vue';
import BackLink from '../BackLink/BackLink.vue';

import { restaurantsImport } from '@/utils/import';

export default defineComponent({
  name: 'RestaurantDetail',
  components: {
    IconExternalLink,
    BackLink,
  },
  data() {
    return {
      restaurantId: 0 as number,
      restaurantsDailies: restaurantsImport,
      restaurantDailyMenu: {
        name: '',
        link: '',
        soups: [
          {
            name: '',
            price: '',
          },
        ],
        mainCourses: [
          {
            name: '',
            price: '',
          },
        ],
        salads: [
          {
            name: '',
            price: '',
          },
        ],
        deserts: [
          {
            name: '',
            price: '',
          },
        ],
      },
    };
  },
  created() {
    this.restaurantId = parseInt(this.$route.params.id.toString(), 10);
  },
  methods: {
    defineRestaurant(id: number) {
      return this.restaurantsDailies.restaurants[id];
    },
  },
});

