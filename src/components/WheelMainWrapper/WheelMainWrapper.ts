import { defineComponent } from 'vue';

import WheelListItem from '../WheelListItem/WheelListItem.vue';

export default defineComponent({
  name: 'WheelMainWrapper',
  components: {
    WheelListItem,
  },
  data() {
    return {
      wheelRotationDeg: 0,
      restaurantsTest: [
        {
          name: 'Husinec',
          link: 'https://www.zomato.com/praha/husinec-nov%C3%A9-m%C4%9Bsto-praha-1/daily-menu',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Ferdinanda',
          link: 'http://www.ferdinanda.cz/cs/nove-mesto/menu/denni-menu',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Jiná Krajina',
          link: 'https://www.menicka.cz/2344-jina-krajina.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Jáma restaurace',
          link: 'https://www.menicka.cz/6436-jama-restaurace.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'U Provaznice',
          link: 'https://www.menicka.cz/1837-u-provaznice.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Veget bistro café',
          link: 'https://www.menicka.cz/4505-veget-bistro-cafe.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Krušovická Šalanda',
          link: 'https://www.menicka.cz/7025-krusovicka-salanda-.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Příčný řez',
          link: 'https://www.menicka.cz/506-pricny-rez.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Restaurace U Pravdů',
          link: 'https://www.menicka.cz/4584-restaurace-u-pravdu.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'The PUB',
          link: 'https://www.menicka.cz/2139-the-pub.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Šenkovna PUB',
          link: 'https://www.menicka.cz/7851-senkovna-pub.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Mechanický Balet',
          link: 'https://www.menicka.cz/4424-mechanicky-balet.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
        {
          name: 'Titanic steak & beer',
          link: 'https://www.menicka.cz/1934-titanic-steak--beer.html',
          soups: [],
          mainCourses: [],
          salads: [],
          deserts: [],
        },
      ],
    };
  },
  methods: {
    addRotation(maxItems: number, itemNumber: number) {
      return (360 / maxItems) * itemNumber;
    },
    addTranslation(maxItems: number, itemHeight: string) {
      return `translateZ(calc((${maxItems - 1} * (${itemHeight})) / (2 * ${
        Math.PI
      })))`;
    },
    addWrapperHeight(maxItems: number, itemHeight: string) {
      return `calc((${maxItems} * (${itemHeight})) / ${Math.PI})`;
    },
    spinTheWheel() {
      const addDegreesOfRotation = Math.floor(Math.random() * 360) + 1080;

      this.wheelRotationDeg += addDegreesOfRotation;

      return this.wheelRotationDeg;
    },
  },
});
