import { defineComponent } from 'vue';

import BackLink from '../BackLink/BackLink.vue';

export default defineComponent({
  name: 'SearchResult',
  components: {
    BackLink,
  },
  data() {
    return {
      result: null,
    };
  },
});

