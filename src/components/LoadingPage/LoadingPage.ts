import { defineComponent } from 'vue';

import IconCookingPot from '@/components/Icons/IconCookingPot/IconCookingPot.vue';

export default defineComponent({
  name: 'LoadingPage',
  components: {
    IconCookingPot,
  },
});

