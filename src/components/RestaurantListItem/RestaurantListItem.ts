import { defineComponent } from 'vue';

export default defineComponent({
  name: 'RestaurantListItem',
  props: {
    restaurantName: {
      type: String,
      default: '',
    },
    restaurantId: {
      type: Number,
      default: 0,
    },
  },
});

