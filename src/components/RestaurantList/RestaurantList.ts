import { defineComponent } from 'vue';

import { restaurantsImport } from '@/utils/import';

import RestaurantListItem from '../RestaurantListItem/RestaurantListItem.vue';

export default defineComponent({
  name: 'RestaurantList',
  components: {
    RestaurantListItem,
  },
  data() {
    return {
      dailyMenusData: restaurantsImport,
    };
  },
});

