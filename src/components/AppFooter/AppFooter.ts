import { defineComponent } from 'vue';

import IconBurger from '../Icons/IconBurger/IconBurger.vue';

export default defineComponent({
  name: 'AppFooter',
  components: {
    IconBurger,
  },
  props: {
    isOpened: Boolean,
  },
});

