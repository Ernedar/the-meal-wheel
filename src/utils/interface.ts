export interface Meal {
  name: string;
  price: string;
}

export interface Soups {
  [index: number]: Meal;
}

export interface MainCourses {
  [index: number]: Meal;
}

export interface Salads {
  [index: number]: Meal;
}

export interface Deserts {
  [index: number]: Meal;
}

export interface RestaurantItem {
  name: string;
  link: string;
  soups: Soups;
  mainCourses: MainCourses;
  salads: Salads;
  deserts: Deserts;
}

export interface RestaurantList {
  [index: number]: RestaurantItem;
}

export interface DailyMenuLists {
  dayToday: string;
  restaurants: RestaurantList;
}

