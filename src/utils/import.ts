import { DailyMenuLists } from './interface';

import restaurantMenus from '../assets/restaurantMenus.json';

export const restaurantsImport: DailyMenuLists = restaurantMenus;
