import { defineAsyncComponent } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

import LoadingPage from '@/components/LoadingPage/LoadingPage.vue';
import NotFound from '@/components/NotFound/NotFound.vue';

const SearchPage = () => import('@/components/SearchPage/SearchPage.vue');

const SearchResult = defineAsyncComponent({
  loader: () => import('@/components/SearchResult/SearchResult.vue'),
  loadingComponent: LoadingPage,
  delay: 100,
  errorComponent: NotFound,
  timeout: 5000,
});

const MealWheel = defineAsyncComponent({
  loader: () => import('@/components/WheelMainWrapper/WheelMainWrapper.vue'),
  loadingComponent: LoadingPage,
  delay: 100,
  errorComponent: NotFound,
  timeout: 5000,
});

const RestaurantList = defineAsyncComponent({
  loader: () => import('@/components/RestaurantList/RestaurantList.vue'),
  loadingComponent: LoadingPage,
  delay: 100,
  errorComponent: NotFound,
  timeout: 5000,
});

const RestaurantDetail = defineAsyncComponent({
  loader: () => import('@/components/RestaurantDetail/RestaurantDetail.vue'),
  loadingComponent: LoadingPage,
  delay: 100,
  errorComponent: NotFound,
  timeout: 5000,
});

export const routes = [
  {
    path: '/',
    name: 'MealWheel',
    component: MealWheel,
    replace: true,
  },
  {
    path: '/search',
    name: 'SearchPage',
    component: SearchPage,
    replace: true,
  },
  {
    path: '/result',
    name: 'SearchResult',
    component: SearchResult,
    replace: true,
  },
  {
    path: '/restaurants',
    name: 'RestaurantList',
    component: RestaurantList,
    replace: true,
  },
  {
    path: '/restaurants/:id',
    name: 'RestaurantDetail',
    component: RestaurantDetail,
    replace: true,
  },
  {
    path: '/loading',
    name: 'LoadingPage',
    component: LoadingPage,
    replace: true,
  },
  {
    path: '/:catchAll(.*)*',
    name: 'NotFound',
    component: NotFound,
    replace: true,
  },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});

